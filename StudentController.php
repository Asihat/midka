<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Student;
use Redirect;
use Input;
class StudentController extends Controller{

	public function index(){
		//echo "this is index";
		//return view('student_index');
		
		$student = Student::paginate(5);
		//echo '<pre>';
		//print_r($student -> toArray());	
		//die();
		return view('student_index', compact('student') );	
	}
	public function create(){
		return view('student_create');
	}
	// insert data
	public function store(Request $request){
		//print_r($request -> all());
		//echo $request -> student_name; 
		//die();		
		$student            = new Student();
		$student -> name    = $request -> student_name;
		$student -> address = $request -> student_address;
		$student -> save();
		return redirect() -> route('student.index');	
	}
	public function edit($id)
	{
		$student = Student::find($id);
		//print_r($student -> toArray());
		//die();	
		return view('student_edit', compact('student'));
	}
	public function update(Request $request, $id)
	{
		//echo $id;
		//echo '<pre>';
		//print_r($request -> all());
		$student = Student::find($id);
		$student -> name    = $request -> student_name;
		$student -> address = $request -> student_address;
		$student -> save();
		return redirect() -> route('student.index');	
	}
	public function destroy($id)
	{
		$student = Student::find($id);
		$student -> delete();
		return redirect() -> route('student.index');
	}
}
?>