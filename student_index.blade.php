<a href='<?php echo route('student.create'); ?>' >Create</a>
<br><br>
<table border="1">
	<tr>
		<th>No</th>
		<th>Title</th>
		<th>text</th>
		<th>Action</th>
	</tr>
	<tr>
	<?php
	$s = 1;
		foreach($student as $key => $value){
	?>
	<tr>
		<td><?php echo $value -> id?></td>		
		<td><?php echo $value -> name    ?></td>
		<td><?php echo $value -> address ?></td>
		<td>
			<a href='<?php echo route('student.edit', $value -> id); ?>' >Edit</a>
			&nbsp; 
			<form action="<?php echo route('student.destroy', $value -> id); ?>" method="post" style="display: inline-block;">
			<?php echo method_field('DELETE'); ?>			
			<?php echo csrf_field();  ?>			
			 
				<a href="javascript:;" onclick="confirm_delete(this.parentNode)" > Delete </a>
			</form>
		</td>
	</tr>		
	<?php
	}
	?>
	</tr>
</table>

{!! $student->render() !!}
<style type="text/css">
li{
	list-style: none;
	display: inline-block;
}
</style>
<script type="text/javascript">
	function confirm_delete(form)
	{
		c = confirm('Delete data?');
		//alert("test");
		if(c == true)
		{
			form.submit();
		}else
		{
			//do nothing	
		}
			
	}
</script>